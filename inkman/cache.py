#
# Copyright 2019 Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>
#
"""
Basic caching infrastructure.
"""

import os
import json

# TBD: None of this will work for windows users
CACHE_DIR = os.path.expanduser('~/.cache/inkscape/extensions')

class CacheManager(object):
    """
    A simple dir based cache manager
    """
    def __init__(self, suffix=None, path=CACHE_DIR):
        if suffix is not None:
            path = os.path.join(path, suffix)
        self.path = path

    def get_filename(self, name, create=False):
        """Convert the given name into a filename"""
        filename = os.path.join(self.path, name)
        if create:
            path = os.path.dirname(filename)
            if not os.path.isdir(path):
                os.makedirs(path)
        return filename

    def set_cache(self, name, data):
        """Save the data into the cache"""
        if isinstance(data, dict):
            try:
                data = json.dumps(data)
                name += '.json'
            except ValueError:
                pass
        if not isinstance(data, (bytes, str)):
            raise TypeError("Cached data must be string or bytes.")
        with open(self.get_filename(name, create=True), 'wb') as fhl:
            if not isinstance(data, bytes):
                data = data.encode('utf-8')
            fhl.write(data)

    def get_cache(self, name):
        """Get the cache named, returns None if not cache set"""
        filename = self.get_filename(name)

        is_json = False
        if os.path.isfile(filename + '.json'):
            is_json = True
            filename += '.json'

        if os.path.isfile(filename):
            with open(filename, 'rb') as fhl:
                ret = fhl.read()
                if is_json:
                    ret = json.loads(ret)
                return ret
        return None

    def cache_or(self, name, func, *args, **kwargs):
        """
        Attempts to get the cache, if it fails, calls the function
        and then saves the result in the cache, passing it back
        """
        data = self.get_cache(name)
        if data is None:
            data = func(*args, **kwargs)
            if data is not None:
                self.set_cache(name, data)
        return data


