#
# Copyright 2018 Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>
#
"""
A simple extension package creator
"""

import os
import sys

from gtkme import ChildWindow
from gtkme.pixmap import PixmapManager

class ExtensionCreator(ChildWindow):
    """Show a simple creator for the selected inx files"""
    name = 'creator'

    def save_package(self, widget):
        pass
        # 1. Select SaveAs file using GtkFileDialog
        # 2. Generate setup.py and other files (README, LICENSE, etc)
        # 3. Compile inx and py files into zip
        # 4. Save out everything
        self.destroy()

    def load_widgets(self, files):
        pass
        # TODO:
        # 1. Open each InxFile, making sure it's valid.
        # 2. Check consistancy, that py files and other files are available
        # 3. Ask user for package information (widgets?)
        # 4. Ask user for SVG Icon, make sure it's sized right
        # 5. Generate setup.py using information
        # 6. Use sdist to create a source package

    def add_file(self, widget):
        pass

    def remove_file(self, widget):
        pass

    def add_dep(self, widget):
        pass

    def remove_dep(self, widget):
        pass

    def select_readme(self, widget):
        pass

    def select_icon(self, widget):
        pass

