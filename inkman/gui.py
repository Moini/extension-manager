#
# Copyright 2018 Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>
#
"""
Gtk 3+ Inkscape 1.0 Extensions Manager GUI.
"""

import os
import sys

from inkman.utils import DATA_DIR
from inkman.cache import CacheManager

from gtkme import Window, ChildWindow, GtkApp, TreeView, asyncme
from gtkme.pixmap import PixmapManager
from gtkme.main import Gtk

# Data modules
from inkman.package import PackageLists, CoreExtensions, OrphanExtensions
from inkman.remote import RemoteArchive, FilePackage

from .info import MoreInformation
from .creator import ExtensionCreator

TEST_SERVER = '--test' in sys.argv
CACHE = CacheManager()

class LocalTreeView(TreeView):
    """A List of locally installed packages"""
    def setup(self, *args, **kwargs):
        """Setup the treeview with one or many columns manually"""
        pixmaps = self.args['pixmaps']

        def get_name(item):
            return f"<big><b>{item.name}</b></big>\n<i>{item.summary}</i>"
        def get_version(item):
            vers = item.installables()
            if vers is None or item.version in vers:
                return f"<i>{item.version}</i>"
            if not vers:
                return f"<s><i>{item.version}</i></s>"
            latest = sorted(list(vers))[-1]
            return f"<i>{latest}</i> <s>{item.version}</s>"

        col = self.ViewColumn('Extensions Package', expand=True,\
            text=get_name, template=None,\
            icon=lambda item: pixmaps.get(item.get_icon()),\
            pad=0, size=None)

        col._icon_renderer.set_property('ypad', 2) # pylint: disable=protected-access
        col._text_renderer.set_property('xpad', 8) # pylint: disable=protected-access

        self.ViewColumn('Version', expand=False, text=get_version, template=None, pad=6)

        self.ViewColumn('Author', expand=False,\
            text=lambda item: item.author, pad=6)

        self.ViewSort(data=lambda item: item.name, ascending=True, contains='name')

class RemoteTreeView(LocalTreeView):
    """A List of remote packages for installation"""
    def setup(self, *args, **kwargs):
        pixmaps = self.args['pixmaps']
        self.ViewColumn('Installed', expand=False, text=False, pad=4, size=16,
            icon=lambda item: pixmaps.get(['gtk-no', 'gtk-yes'][bool(item.installed)]))

        super().setup(*args, **kwargs)



class ExtensionManagerWindow(Window):
    name = 'gui'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.pixmaps = PixmapManager('pixmaps', pixmap_dir=DATA_DIR, size=48, load_size=(96,96))
        self.archive = RemoteArchive(test_server=TEST_SERVER)

        self.searching = self.widget('dl-searching')
        self.searchbox = self.searching.get_parent()
        self.searchbox.remove(self.searching)

        self.local = LocalTreeView(
            self.widget('local_extensions'),
            pixmaps=self.pixmaps,
            selected=self.select_local)

        self.remote = RemoteTreeView(
            self.widget('remote_extensions'),
            pixmaps=self.pixmaps,
            selected=self.select_remote)

        self.orphans = OrphanExtensions()
        self.widget('loading').start()
        self.window.show_all()

        if not TEST_SERVER:
            self.widget('test_server_warn').hide()
        self.refresh_local()
        self.widget('remote_install').set_sensitive(False)
        self.widget('local_install').set_sensitive(True)

    def change_local_all(self, widget, unk):
        """When the source switch button is clicked"""
        self.refresh_local()

    @asyncme.run_or_none
    def refresh_local(self):
        """Searches for locally installed extensions and adds them"""
        self.local.clear()
        self.local.add_item([CoreExtensions()])
        filtered = not self.widget('local_all').get_active()
        self.widget('local_uninstall').set_sensitive(False)
        self.widget('local_information').set_sensitive(False)

        all_packages = []
        for package_list in PackageLists():
            for package in package_list:
                if package not in all_packages:
                    self.orphans.remove_files(package.get_files())
                    all_packages.append(package)
                    if not filtered or package.is_extension():
                        self.local.add_item([package])
        if list(self.orphans.get_extensions()):
            self.local.add_item([self.orphans])
        self.widget('loading').stop()

    def select_local(self, item):
        """Select an installed extension"""
        self.widget('local_uninstall').set_sensitive(item.is_writable())
        self.widget('local_information').set_sensitive(bool(item))

    def local_information(self, widget):
        """Show the more information window"""
        if self.local.selected:
            self.load_window('info', pixmaps=self.pixmaps, item=self.local.selected)

    def local_uninstall(self, widget):
        """Uninstall selected extection package"""
        item = self.local.selected
        if item.is_writable():
            if item.uninstall():
                self.local.remove_item(item)
                # In the even it happened to be in a remote search at the same time
                for search_item, treeiter in self.remote:
                    if item.name == search_item.name:
                        search_item.installed = None
                self.remote.refresh()

    def change_remote_all(self, widget, unk):
        """When the source switch button is clicked"""
        self.remote_search(self.widget('dl-search'))

    def remote_search(self, widget):
        """Remote search activation"""
        filtered = not self.widget('remote_all').get_active()
        query = widget.get_text()
        if len(query) > 2:
            self.remote.clear()
            self.widget('remote_install').set_sensitive(False)
            self.widget('dl-search').set_sensitive(False)
            self.searchbox.add(self.searching)
            self.widget('dl-searching').start()
            self.async_search(query, filtered)

    @asyncme.run_or_none
    def async_search(self, query, filtered):
        """Asyncronous searching in PyPI"""
        for package in self.archive.search(query, filtered):
            self.add_search_result(package)
        self.search_finished()

    @asyncme.mainloop_only
    def add_search_result(self, package):
        """Adding things to Gtk must be done in mainloop"""
        self.remote.add_item([package])

    @asyncme.mainloop_only
    def search_finished(self):
        """After everything, finish the search"""
        self.searchbox.remove(self.searching)
        self.widget('dl-search').set_sensitive(True)
        self.replace(self.searching, self.remote)

    def select_remote(self, item):
        """Select an installed extension"""
        # Do we have a place to install packages to?
        active = self.remote.selected \
            and self.remote.selected.is_installable() \
            and not self.remote.selected.installed
        self.widget('remote_install').set_sensitive(active)

    def remote_install(self, widget):
        """Install a remote package"""
        self.widget('remote_install').set_sensitive(False)
        item = self.remote.selected
        ret = item.install()
        self.dialog(ret)
        self.refresh_local()
        self.remote.refresh()

    def local_install(self, widget):
        """Install from a local filename"""
        dialog = Gtk.FileChooserDialog(
            title="Please choose a package file", transient_for=self.window,
            action=Gtk.FileChooserAction.OPEN)

        dialog.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN, Gtk.ResponseType.OK)

        filter_text = Gtk.FileFilter()
        filter_text.set_name("Python Wheel")
        filter_text.add_mime_type("application/zip")
        filter_text.add_pattern("*.whl")
        dialog.add_filter(filter_text)

        filter_py = Gtk.FileFilter()
        filter_py.set_name("Python Packages")
        filter_py.add_mime_type("application/x-compressed-tar")
        dialog.add_filter(filter_py)

        filter_any = Gtk.FileFilter()
        filter_any.set_name("Any files")
        filter_any.add_pattern("*")
        dialog.add_filter(filter_any)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            package = FilePackage(dialog.get_filename())
            package.install()
            self.refresh_local()

        dialog.destroy()

    def local_creator(self, widget):
        """Create a new package from local Inx Files"""
        dialog = Gtk.FileChooserDialog(
            title="Please choose your inx files", transient_for=self.window,
            action=Gtk.FileChooserAction.OPEN)

        dialog.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN, Gtk.ResponseType.OK)

        filter_text = Gtk.FileFilter()
        filter_text.set_name("Inkscape Extension Files")
        filter_text.add_mime_type("text/xml")
        filter_text.add_pattern("*.inx")
        dialog.add_filter(filter_text)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            files = dialog.get_filenames()
            dialog.destroy()
            self.load_window('creator', files=files)
        else:
            dialog.destroy()

    def dialog(self, msg):
        self.widget('dialog_msg').set_label(msg)
        self.widget('dialog').set_transient_for(self.window)
        self.widget('dialog').show_all()

    def close_dialog(self, widget):
        self.widget('dialog_msg').set_label('')
        self.widget('dialog').hide()


class ManagerApp(GtkApp):
    """Load the inkscape extensions glade file and attach to window"""
    glade_dir = os.path.join(os.path.dirname(__file__), 'data')
    app_name = 'inkscape-extensions-manager'
    windows = [ExtensionManagerWindow, MoreInformation, ExtensionCreator]
