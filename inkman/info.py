#
# Copyright 2018 Martin Owens <doctormo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>
#
"""
Gtk 3+ Inkscape 1.0 Extensions Manager GUI.
"""

import os
import sys

from gtkme import ChildWindow, TreeView

class ExtensionTreeItem(object):
    """Shows the name of the item in the extensions tree"""
    kind = 'debug'

    def __init__(self, name):
        self.name = str(name)


class ExtensionTreeView(TreeView):
    """A list of extensions (inx file based)"""
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.parents = {}
        self.menus = {}

    def setup(self, *args, **kwargs):
        self.ViewColumn('Name', expand=True, text='name')
        self.ViewSort(data=lambda item: item.name, ascending=True)

    def get_menu(self, parent, *remain):
        menu_id = '::'.join([str(r) for r in remain])
        if menu_id in self.menus:
            return self.menus[menu_id]

        if remain[:-1]:
            parent = self.get_menu(parent, *remain[:-1])

        menu = self._add_item([ExtensionTreeItem(remain[-1])], parent=parent)
        self.menus[menu_id] = menu
        return menu

    def add_item(self, item, parent=None):
        if not item or not item.name:
            return None
        if item.kind not in self.parents:
            tree_item = ExtensionTreeItem(item.kind.title())
            self.parents[item.kind] = self._add_item([tree_item], parent=None)
        parent = self.parents[item.kind]
        if item.kind == 'effect' and len(item.menu) > 1:
            parent = self.get_menu(parent, *item.menu[:-1])
        return self._add_item([item], parent)


class MoreInformation(ChildWindow):
    """Show further information for an installed package"""
    name = 'info'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.inx = ExtensionTreeView(self.widget('inx'), selected=self.select_inx)

    def load_widgets(self, pixmaps, item):
        """Initialise the information"""
        self.pixmaps = pixmaps
        self.window.set_title(item.name)
        self.widget('info_name').set_label(item.name)
        self.widget('info_desc').set_label(item.summary)
        self.widget('info_version').set_label(item.version)
        self.widget('info_license').set_label(item.license)
        self.widget('info_website').set_uri(item.url)
        self.widget('info_website').set_label(item.url)
        self.widget('info_author').set_label(f"{item.author} <{item.author_email}>")
        self.widget('info_image').set_from_pixbuf(pixmaps.get(item.get_icon()))

        self.inx.clear()
        self.inx.add(item.get_extensions())

    def select_inx(self, item):
        pass

