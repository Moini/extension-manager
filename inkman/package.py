#
# Copyright (C) 2019 Martin Owens
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
"""
Support for constructing and understanding installed extensions
as python packages.

Start by using 'PackageLists' and iterate over to get 'PackageList' objects
iterate over those to get 'Package' objects and use those to know more
information about the installed packages.
"""

import os
import sys
import json
import logging

from inkex.inx import InxFile
from .utils import DATA_DIR, ICON_SEP, parse_metadata, clean_author,\
    get_user_directory, get_inkscape_directory

# The ORDER of these places is important, when installing packages it will
# choose the FIRST writable directory in the lsit, so make sure that the
# more preferable locations appear at the top.
EXTENSION_PLACES = []

for path in sys.path:
    if 'extensions' in path:
        if '/bin' in path:
            path = path.replace('/bin', '')
        EXTENSION_PLACES.append(path)

CORE_ICON = os.path.join(DATA_DIR, 'core_icon.svg')
ORPHAN_ICON = os.path.join(DATA_DIR, 'orphan_icon.svg')
DEFAULT_ICON = os.path.join(DATA_DIR, 'default_icon.svg')
MODULE_ICON = os.path.join(DATA_DIR, 'module_icon.svg')

EXTENSION_FILTERS = {
    'classifiers': [
        'Environment :: Plugins',
        'Topic :: Multimedia :: Graphics :: Editors :: Vector-Based',
    ]
}

class PackageLists(object):
    """
    A list of package lists where extensions are found.
    """
    def __iter__(self):
        for loc in EXTENSION_PLACES:
            loc = os.path.abspath(os.path.expanduser(loc))
            if os.path.isdir(loc):
                yield PackageList(loc)

    def get_package(self, name, version=None):
        """Test every packages list for the given package name, test version"""
        for packages in self:
            found = packages.get_package(name, version=version)
            if found:
                return found
        return None


class PackageList(object):
    """
    A list of packages based on a target directory
    """
    def __init__(self, path):
        self.path = os.path.abspath(path)

    def __str__(self):
        if self.is_user():
            user_dir = get_user_directory().rstrip('/')
            short_path = self.path.replace(user_dir, '~')
            return f"User extensions {short_path}"
        return f"System extension {self.path}"

    def is_user(self):
        """Return true if these are system extensions"""
        user_dir = get_user_directory().rstrip('/')
        return user_dir and self.path.startswith(user_dir)

    def __repr__(self):
        return f"<PackageList '{self}'>"

    def __iter__(self):
        return self.iter()

    def iter(self):
        for node in self.get_package_paths():
            if node.endswith('.dist-info') or node.endswith('.egg-info'):
                # Skip packages installed by virtualenv
                venv_file = node.rsplit('.', 1)[0] + '.virtualenv'
                if os.path.isfile(venv_file):
                    continue
                package = InstalledPackage(os.path.join(self.path, node), self.path)
                yield package

    def get_package_paths(self):
        pyver = "python" + sys.version[:3]
        for varient in [
                os.path.join(self.path, "lib", pyver, "site-packages"),
            ]:
            if os.path.isdir(varient):
                for subpath in os.listdir(varient):
                    yield os.path.join(varient, subpath)

    def is_writable(self):
        """Can the folder be written to (for installation)"""
        return os.access(self.path, os.W_OK)

    def get_package(self, name, version=None):
        """Test every package in this list if it matches the name and version"""
        for package in self.iter():
            found = package.is_package(name, version=version)
            if found:
                return package
        return None


class ProtoPackage(object):
    """
    A single package, either wheel dist or egg.
    """
    url = property(lambda self: self.get_metadata()['url'] or '')
    name = property(lambda self: self.get_metadata()['name'] or '')
    summary = property(lambda self: self.get_metadata()['summary'] or '')
    version = property(lambda self: self.get_metadata()['version'] or '')
    license = property(lambda self: self.get_metadata()['license'] or '')
    author = property(lambda self: self.get_metadata()['author'] or '')
    author_email = property(lambda self: self.get_metadata()['author_email'] or '')
    default_icon = DEFAULT_ICON
    path = None
    writable = False

    def __init__(self):
        self._files = None

    def __str__(self):
        return self.name

    def is_package(self, name, version=None):
        """Test if this package matches the given name and version"""
        mdat = self.get_metadata()
        if mdat['name'] == name:
            if version is None or self.is_version(version):
                return True
        return False

    def is_version(self, version):
        """Test if this package version is up to date"""
        # XXX We could do with using the real version checker from python packages
        if self.version >= version:
            return True
        return False

    def is_extension(self):
        """Returns True if this is an inkscape extension"""
        mdat = self.get_metadata()
        for clf in EXTENSION_FILTERS['classifiers']:
            if clf not in mdat['classifiers']:
                return False
        return True

    def is_writable(self):
        """Return true if this package is writable (aka, removable)"""
        return self.writable

    def get_metadata(self):
        """Returns the metadata from an array of known types"""
        raise NotImplementedError("get_metadata is needed in child class.")

    def installables(self):
        return self.get_metadata().get('installable', None)

    def get_icon(self):
        """Returns the icon associated with this package"""
        return self.default_icon

    def get_files(self):
        if self._files is None:
            self._files = list(self._get_files())
        return self._files

    def _get_files(self):
        """Lists all files in the core installed extensions"""
        if not self.path or not os.path.isdir(self.path):
            return []
        for (dirpath, dirnames, filenames) in os.walk(self.path):
            for filename in filenames:
                yield os.path.join(dirpath, filename)

    def get_file(self, name):
        """Get filename if it exists"""
        if not self.path or not os.path.isdir(self.path):
            return None
        path = os.path.join(self.path, name)
        if os.path.isfile(path):
            with open(path, 'r') as fhl:
                return fhl.read()
        return None

    def get_extensions(self):
        """Returns a list of extensions in this package"""
        for fname in self.get_files():
            if fname.endswith('.inx'):
                try:
                    yield ExtensionInx(fname)
                except OSError:
                    pass # bad inx file

    def uninstall(self):
        """Delete the package from the target directory"""
        if not self.is_writable():
            return False
        try:
            for fname in self.package_files():
                self._crop_dirs(fname)
            return True
        except IOError:
            return False

    def is_file_protected(self, path):
        return False

    def _crop_dirs(self, path):
        if self.is_file_protected(path):
            return
        try:
            if os.path.isfile(path):
                os.unlink(path)
                logging.debug(f" [X] {path} (file)")
            elif os.path.isdir(path):
                os.rmdir(path)
                logging.debug(f" [X] {path} (folder)")
            else:
                logging.debug(f" [X] {path} (gone)")
                return # Already deleted

            self._crop_dirs(os.path.dirname(path))
        except (IOError, OSError):
            pass

    def get_description(self):
        """Returns the description from the file or the metadata"""
        return self.get_metadata().get('description', None)

class Package(ProtoPackage):
    """An actual package, local or remote"""
    def get_icon(self):
        """Get icon from description"""
        desc = self.get_description()
        if desc and ICON_SEP in desc:
            return desc.split(ICON_SEP)[-1].strip()
        icon = self.default_icon if self.is_extension() else MODULE_ICON
        with open(icon, 'r') as fhl:
            return fhl.read()

class CoreExtensions(ProtoPackage):
    """
    Represent Inkscape's internal extensions
    """
    default_icon = CORE_ICON
    path = get_inkscape_directory()

    def get_metadata(self):
        return {
            'version': '1.0',
            'name': "Inkscape Core Extensions",
            'summary': "The extensions shipped with Inkscape itself",
            'author': "The Inkscape Developers",
            'author_email': "devel@lists.inkscape.org",
            'description': "Full README",
            'license': "GPLv3",
            'url': "https://gitlab.com/inkscape/extensions/",
            'classifiers': EXTENSION_FILTERS,
        }

class OrphanExtensions(ProtoPackage):
    """A list of extensions not a part of a package"""
    default_icon = ORPHAN_ICON
    path = get_user_directory()
    writable = True

    def __init__(self):
        super().__init__()
        # Record all files upfront
        self.get_files()

    def remove_files(self, files):
        """Remove files which are part of known packages"""
        for fname in files:
            try:
                self._files.remove(fname)
            except ValueError:
                pass

    def get_metadata(self):
        return {
            'version': 'N/A',
            'name': "Orphan Extensions",
            'summary': "Old and manually installed extensions",
            'author': "Various",
            'author_email': "N/A",
            'description': "N/A",
            'license': "Unknown",
            'url': '',
            'classifiers': EXTENSION_FILTERS,
        }

class InstalledPackage(Package):
    """
    A package in a specific location.
    """
    def __init__(self, info_path, container=''):
        super().__init__()
        if not os.path.isdir(info_path):
            raise IOError("Can't find package directory: {}".format(info_path))
        self._metadata = None
        self.path = info_path.rstrip('/')
        self.base = os.path.basename(self.path)
        self.dirname = os.path.dirname(self.path)
        self.container = container.rstrip('/')

    def __repr__(self):
        return "<InstalledPackage '{}' installed to '{}'>".format(str(self), self.dirname)

    def __eq__(self, other):
        return other is not None and other.path == self.path

    def is_writable(self):
        """Return true if this package is writable (and deletable)"""
        return os.access(self.path, os.W_OK)

    def get_metadata(self):
        """Returns the metadata from an array of known types"""
        if self._metadata is None:
            for name in ('METADATA', 'PKG-INFO'):
                md_mail = self.get_file(name)
                if md_mail:
                    self._metadata = parse_metadata(md_mail)
        if self._metadata is None:
            md_json = self.get_file('metadata.json')
            if md_json:
                self._metadata = clean_author(json.loads(md_json))
        if self._metadata is None:
            raise KeyError("Can't find package meta data: {}".format(self.path))
        return self._metadata

    def get_description(self):
        """Returns the description from the file or the metadata"""
        desc = self.get_file('DESCRIPTION.rst')
        if not desc:
            return super(InstalledPackage, self).get_description()
        return desc

    def _get_files(self):
        """Generates a list of files associated with this package,
        if pkg is true, then it also yields pkg meta data files"""
        for filename in self.package_files():
            if not filename.startswith(self.base):
                yield filename

    def package_files(self):
        """Return a generator of all files in this installed package"""
        record = self.get_file('RECORD')
        if not record:
            return
        for line in record.split('\n'):
            if line and ',' in line:
                (filename, checksum, size) = line.rsplit(',', 2)
                # XXX Check filesize or checksum?
                yield os.path.abspath(os.path.join(self.dirname, filename))

    def is_file_protected(self, path):
        """Protect files outside of the python/pip container"""
        if self.container:
            if not path.startswith(self.container) or path.rstrip('/') == self.container:
                return True
        return False

class ExtensionInx(InxFile):
    """Information about an extension specifically"""
    ident = property(lambda self: super().ident or f"[no-id] {self.filename}")
    name = property(lambda self: super().name or f"[unnamed] {self.ident}")

    @property
    def kind(self):
        template = self.root.xpath('inkscape:templateinfo',
            # patch for older inkex.inx modules
            namespaces={'inkscape': 'http://www.inkscape.org/namespaces/inkscape'})
        if template:
            return 'template'
        try:
            return super().kind
        except KeyError:
            return 'bad'

    @property
    def menu(self):
        menu = self.find_one('inx:effect/inx:effects-menu')
        if menu is not None and menu.get('hidden', "false") == "true":
            return ["_hidden", self.name]
        return super().menu
