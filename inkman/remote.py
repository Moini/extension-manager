#
# Copyright (C) 2019 Martin Owens
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
"""
Searching for external packages and getting meta data about them.
"""

import re
import os
import json
import logging

from inkex.command import CommandNotFound, ProgramRunError, call
from inkex.inkscape_env import get_bin
from collections import defaultdict

from pip._internal.models.index import PyPI
from pip._vendor.distlib.locators import Locator
from pip._vendor.distlib.compat import urljoin, quote
from pip._vendor.distlib.util import ensure_slash

from .package import Package, PackageLists
from .cache import CacheManager

PYTHON_VERSION = 'py3'
SERVERS = [
    PyPI.pypi_url, # Live PyPi
    "https://test.pypi.org/pypi/", # Test PyPi
]
SERVER_NAME = ['live', 'test']
PIP_COMMAND = get_bin('pip')

class RemoteArchive(object):
    """
    Control calling out to pip to search for extensions in the package index.
    """
    SEARCH_REX = re.compile(r'(?P<name>.+) \((?P<ve>[\d\.]+)\) +- (?P<desc>.+)')

    def __init__(self, **kwargs):
        self.test_server = kwargs.pop('test_server', False)
        self.index = kwargs.pop('index', SERVERS[self.test_server])
        self.cache = CacheManager(suffix=SERVER_NAME[self.test_server])
        self.cache_dir = os.path.join(self.cache.path, 'archive')

    def search(self, query, filtered=True):
        """
        Search for extension packages

        If filtered is True, only shows extension packages
        """
        results = call(PIP_COMMAND, 'search',
            ('index', self.index), ('isolated', True),
            ('disable-pip-version-check', True),
            ('cache-dir', self.cache_dir), query).decode('utf8')
        for name, version, desc in self.SEARCH_REX.findall(results):
            package = RemotePackage(self, name, version)
            if not filtered or package.is_extension():
                yield package

class InstallableMixin(object):
    def install(self):
        """Install this remote package to the local directory"""
        try:
            results = call(PIP_COMMAND, 'install',
                ('index-url', self.index), ('isolated', True),
                ('disable-pip-version-check', True),
                ('cache-dir', self.cache_dir), self.pkg_name).decode('utf8')
        except ProgramRunError as err:
            raise IOError(str(err))
        self.installed = PackageLists().get_package(self.name)
        return results


class FilePackage(InstallableMixin):
    def __init__(self, filename):
        if not os.path.isfile(filename):
            raise IOError("Can not find file: {}".format(filename))
        self.index = None
        self.pkg_name = filename
        self.name = os.path.basename(filename)


class RemotePackage(InstallableMixin, Package):
    """
    Gets information about the remote package using requests.
    """
    def __init__(self, archive, name, version):
        self.index = archive.index
        self._metadata = None
        self.pkg_name = name
        self.pkg_version = version
        self.cache_name = "{0.pkg_name}-{0.pkg_version}".format(self)
        self.cache_dir = archive.cache_dir
        self.cache = archive.cache
        self.locator = MetadataLocator(self.index)
        self.installed = PackageLists().get_package(self.name)

    def is_installable(self):
        """Return true if it's installable"""
        return bool(self.get_metadata()['installable'])

    def get_metadata(self):
        """Meta data is a directly added item"""
        if self._metadata is None:
            mdat = self.cache.cache_or(
                self.cache_name,
                self.locator.get_metadata, self.pkg_name,
            )
            self._metadata = mdat['info']
            self._metadata['releases'] = mdat['releases']
            self._metadata['installable'] = self._get_installable(mdat['releases'])

        return self._metadata

    def _get_installable(self, releases):
        results = defaultdict(list)
        for (version, details) in releases.items():
            for detail in details:
                if detail['python_version'] in ('source', PYTHON_VERSION):
                    results[version].append(detail)
        return results

class MetadataLocator(Locator):
    """Find the metadata for a package"""
    def __init__(self, index_url):
        super(MetadataLocator, self).__init__()
        self.base_url = ensure_slash(index_url)

    def get_distribution_names(self):
        raise NotImplementedError("Does not return distriction names")

    def get_metadata(self, name):
        """Returns the raw metadata parsed json"""
        url = urljoin(self.base_url, '%s/json' % quote(name))
        try:
            resp = self.opener.open(url)
            data = resp.read().decode() # for now
            return json.loads(data)
        except Exception as err: # pylint: disable=broad-except
            logging.error("Failed to get JSON: %s < %s", str(err), url)
        return None
